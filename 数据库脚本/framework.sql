/*
MySQL Data Transfer
Source Host: localhost
Source Database: framework
Target Host: localhost
Target Database: framework
Date: 2014/1/15 1:04:49
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for department
-- ----------------------------
CREATE TABLE `department` (
  `id` varchar(50) NOT NULL,
  `pid` varchar(50) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for function
-- ----------------------------
CREATE TABLE `function` (
  `FUNCTIONID` varchar(50) NOT NULL,
  `FUNCTIONNAME` varchar(50) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`FUNCTIONID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
CREATE TABLE `menu` (
  `MENUID` varchar(50) NOT NULL,
  `MENUNAME` varchar(50) DEFAULT NULL,
  `PARENTMENU` varchar(50) DEFAULT NULL,
  `FUNCTIONID` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`MENUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for role
-- ----------------------------
CREATE TABLE `role` (
  `ROLEID` varchar(50) NOT NULL,
  `ROLENAME` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ROLEID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for role_menu
-- ----------------------------
CREATE TABLE `role_menu` (
  `ID` varchar(50) NOT NULL DEFAULT '',
  `ROLEID` varchar(50) DEFAULT NULL,
  `MENUID` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user
-- ----------------------------
CREATE TABLE `user` (
  `id` varchar(50) NOT NULL,
  `userid` varchar(50) CHARACTER SET big5 DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `department_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=REDUNDANT;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
CREATE TABLE `user_role` (
  `ID` varchar(50) NOT NULL,
  `USERID` varchar(50) DEFAULT NULL,
  `ROLEID` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `department` VALUES ('10061c65-d05f-4e40-bd5c-16edd8b3ce96', '', '六庄小学');
INSERT INTO `department` VALUES ('2e35fca1-9e0f-4356-a3ce-30ffd9ea8cc5', '2fa9d753-7b89-4303-8229-4eeee57035c7', '五年级1班');
INSERT INTO `department` VALUES ('2fa9d753-7b89-4303-8229-4eeee57035c7', '10061c65-d05f-4e40-bd5c-16edd8b3ce96', '五年级');
INSERT INTO `department` VALUES ('454592fa-6235-40df-89d2-cc7bc740072a', '10061c65-d05f-4e40-bd5c-16edd8b3ce96', '二年级');
INSERT INTO `department` VALUES ('4e3d78bc-5642-40bd-880c-c154f8ae1293', '10061c65-d05f-4e40-bd5c-16edd8b3ce96', '六年级');
INSERT INTO `department` VALUES ('5332ecd0-c4ba-44c3-a3aa-8f761ede623b', '10061c65-d05f-4e40-bd5c-16edd8b3ce96', '三年级');
INSERT INTO `department` VALUES ('8d9c1854-6d57-49c8-a222-e64967ae209f', '10061c65-d05f-4e40-bd5c-16edd8b3ce96', '四年级');
INSERT INTO `department` VALUES ('900c3ae2-4ccc-4453-adbd-92950db65834', '5332ecd0-c4ba-44c3-a3aa-8f761ede623b', '三年级1班');
INSERT INTO `department` VALUES ('98d2cb07-a3d4-43ae-b2d9-3eda598134fd', 'b27fbbd1-4965-4ccd-8cc6-14c8cc6e4a66', '一年级1班');
INSERT INTO `department` VALUES ('b27fbbd1-4965-4ccd-8cc6-14c8cc6e4a66', '10061c65-d05f-4e40-bd5c-16edd8b3ce96', '一年级');
INSERT INTO `department` VALUES ('c4c835f7-be84-4185-8cea-1fc728016b68', '8d9c1854-6d57-49c8-a222-e64967ae209f', '四年级1班');
INSERT INTO `department` VALUES ('db182758-805d-45a7-9d82-968c8fc44add', '4e3d78bc-5642-40bd-880c-c154f8ae1293', '六年级1班');
INSERT INTO `department` VALUES ('eb066fe4-f04d-47df-866c-f957a984ebc7', '454592fa-6235-40df-89d2-cc7bc740072a', '二年级1班');
INSERT INTO `function` VALUES ('5b58faed-3593-4a9f-8b73-76c389b896d8', '功能管理', '/framework/ums/function/functionList.jsp');
INSERT INTO `function` VALUES ('5c45602e-1170-483d-8ebb-11483d84ad54', '机构管理', '/framework/ums/department/departmentList.jsp');
INSERT INTO `function` VALUES ('5d9b17f8-0ece-4da8-b107-f452a72318fa', '菜单管理', '/framework/ums/menu/menuList.jsp');
INSERT INTO `function` VALUES ('64758b2c-aa67-43b7-bba5-b5bb3a63201a', '角色管理', '/framework/ums/role/roleList.jsp');
INSERT INTO `function` VALUES ('9076d799-65a5-4ac9-b847-e2155714ab39', '用户管理', '/framework/ums/user/userList.jsp');
INSERT INTO `menu` VALUES ('14b82cb9-fcc5-4fef-b87f-33bf4059f56d', '角色管理', '31cc742e-0548-41ad-b5fa-780443152f1d', '64758b2c-aa67-43b7-bba5-b5bb3a63201a');
INSERT INTO `menu` VALUES ('1904e310-b3b9-4d80-8c15-183a0ee8b1de', '功能管理', '31cc742e-0548-41ad-b5fa-780443152f1d', '5b58faed-3593-4a9f-8b73-76c389b896d8');
INSERT INTO `menu` VALUES ('31cc742e-0548-41ad-b5fa-780443152f1d', '根菜单', '', '');
INSERT INTO `menu` VALUES ('456d4364-fca7-4090-906a-2cbcac23ee65', '菜单管理', '31cc742e-0548-41ad-b5fa-780443152f1d', '5d9b17f8-0ece-4da8-b107-f452a72318fa');
INSERT INTO `menu` VALUES ('96fafa32-5d6a-409e-91a2-3d3d501667e8', '机构管理', '31cc742e-0548-41ad-b5fa-780443152f1d', '5c45602e-1170-483d-8ebb-11483d84ad54');
INSERT INTO `menu` VALUES ('e35298e4-593d-4513-a932-e7279c0178eb', '用户管理', '31cc742e-0548-41ad-b5fa-780443152f1d', '9076d799-65a5-4ac9-b847-e2155714ab39');
INSERT INTO `role` VALUES ('42833c1b-a3af-4f7a-8bdd-4bed0a7f085a', '管理员');
INSERT INTO `role` VALUES ('fe69da06-23bb-4ff4-b792-299ea487ad55', 'VIP用户');
INSERT INTO `role_menu` VALUES ('02eae7da-c805-4e02-8ac1-47083e81d0dc', '42833c1b-a3af-4f7a-8bdd-4bed0a7f085a', 'e35298e4-593d-4513-a932-e7279c0178eb');
INSERT INTO `role_menu` VALUES ('48623d2a-0982-4bd9-acf6-801f7e97f506', '42833c1b-a3af-4f7a-8bdd-4bed0a7f085a', '96fafa32-5d6a-409e-91a2-3d3d501667e8');
INSERT INTO `role_menu` VALUES ('55456949-14fc-4d14-b911-663abacea73a', '42833c1b-a3af-4f7a-8bdd-4bed0a7f085a', '1904e310-b3b9-4d80-8c15-183a0ee8b1de');
INSERT INTO `role_menu` VALUES ('6b6bd434-c676-464d-a432-9d594a54f884', '42833c1b-a3af-4f7a-8bdd-4bed0a7f085a', '456d4364-fca7-4090-906a-2cbcac23ee65');
INSERT INTO `role_menu` VALUES ('aa0093a0-2151-4754-a2ae-c3d9652f024b', '42833c1b-a3af-4f7a-8bdd-4bed0a7f085a', '31cc742e-0548-41ad-b5fa-780443152f1d');
INSERT INTO `role_menu` VALUES ('cd1423e8-fe25-4854-9785-7582cd15f2c4', '42833c1b-a3af-4f7a-8bdd-4bed0a7f085a', '14b82cb9-fcc5-4fef-b87f-33bf4059f56d');
INSERT INTO `user` VALUES ('1eaac623-1a7b-48b6-a9ab-11ee1288e1d9', 'manager', '开发用户', '1', null);
INSERT INTO `user` VALUES ('2b9920e3-0a15-4ab9-a481-15f78ae8b88d', 'lzh', '哈哈', null, null);
INSERT INTO `user_role` VALUES ('73b6abfb-9605-41bd-98c5-a3b758139969', 'manager', '42833c1b-a3af-4f7a-8bdd-4bed0a7f085a');
INSERT INTO `user_role` VALUES ('a041cf2a-d40c-4339-a034-84a0dec2a124', '1eaac623-1a7b-48b6-a9ab-11ee1288e1d9', '42833c1b-a3af-4f7a-8bdd-4bed0a7f085a');
