<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%String contextPath = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户登录</title>
</head>
<body>
	<center>
		<h1>登录</h1>
		<label>账号：</label>
		<input type="text" id="userid" name="userid">
		<label>密码：</label>
		<input type="password" id="password" name="password">
		<input type="button" id="loginBtn" value="登录">
		<br>
		<div id="result"></div>
	</center>
	<script src="<%=contextPath%>/library/js/jquery/jquery-1.3.2.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(function(){
		$("#loginBtn").click(function() {
			login();
		});
	});
	function login() {
		var userid = $("#userid").val();
		var password = $("#password").val();
		var url = '<%=contextPath%>/loginServlet';
		$.post(url,{userid:userid,password:password},function(json){
			var result =json.result;
			if(result) {
				$("#result").html("登录成功");
				window.location.href="<%=contextPath%>/index.jsp";
			}
			else {
				$("#result").html("用户名或者密码错误");
			}
		},"json");
	}
	</script>
</body>
</html>