<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page language="java"  import="util.SpringUtil"%>
<%@ page language="java"  import="ums.service.FunctionService"%>
<%@ page language="java"  import="java.util.List"%>
<%@ page language="java"  import="ums.model.Function"%>
<%String contextPath = request.getContextPath();%>
<%
String functionId = (String)request.getParameter("functionId");
FunctionService functionService = FunctionService.getInstance();
Function function = functionService.getFunctionById(functionId);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=contextPath %>/library/css/style.css"/>
<title>功能编辑</title>
</head>
<body>
功能管理>>功编辑功能
<br>
	<center>
		<form action="<%=contextPath%>/functionEditServlet" method="post">
		<!--<label>是否发布</label><input type="radio">-->
		<label>功能名称</label><input type="text" id="functionName" name = "functionName" value="<%=function.getFunctionName()%>">
		<label>功能URL</label><input style= "width:400px" type="text" id="url" name = "url" value="<%=function.getUrl()%>">
		<input type="hidden" id="functionId" name="functionId" value="<%=function.getFunctionId() %>">
		<input type="button" id="editFunctionBtn" value="编辑功能">
		</form>
		<div id="result">
		</div>
	</center>
	<script language=javascript src="<%=contextPath%>/library/js/jquery/jquery-1.3.2.js"></script>
	<script type="text/javascript">
		$(function() {
			$("#editFunctionBtn").click(function() {
				editFunction();
			});
		});
	
		function editFunction() {
			var functionName = $("#functionName").val();
			var functionId = $("#functionId").val();
			var functionUrl = $("#url").val();
			var url = '<%=contextPath%>/functionEditServlet';
			var data = {
					functionId:functionId,
					functionName:functionName,
					url:functionUrl
			}
			$.post(url,data,function(json){
				var result = json.result;
				if(result) {
					$("#result").html("编辑功能成功");
				}
				else {
					$("#result").html("编辑功能失败");
				}
			},"json");
		}
	</script>
</body>
</html>