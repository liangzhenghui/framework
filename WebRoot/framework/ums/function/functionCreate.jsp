<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%String contextPath = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=contextPath %>/library/css/style.css"/>
<title>功能创建</title>
</head>
<body>
菜单管理>>新建功能
<center>
	<form action="">
		<label>功能名称</label><input type="text" id="functionName" name = "functionName">
		<label>URL路径</label><input type="text" id="url" name = "url">
		<input type = "button" id="createFunctionBtn" value="创建功能">
	</form>
	<div id="result"></div>
</center>
<script language=javascript src="<%=contextPath%>/library/js/jquery/jquery-1.3.2.js"></script>
<script type="text/javascript">
$(function() {
	$("#createFunctionBtn").click(function() {
		createFunction();
	});
});
function createFunction() {
	var functionName = $("#functionName").val();
	if(functionName.length ==0) {
		$("#result").html("功能名称不能为空");
		return;
	}
	var functionUrl = $("#url").val();
	var url = '<%=contextPath%>/functionCreateServlet';
	var data = {
		functionName : functionName,
		url:functionUrl
	};
	$.post(url,data,function(json){
		var result = json.result;
		if(result) {
			$("#result").html("创建功能成功");
		}
		else {
			$("#result").html("创建功能失败");
		}
	},"json");
}
</script>
</body>
</html>