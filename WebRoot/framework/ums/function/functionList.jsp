<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%String contextPath = request.getContextPath();%>
<%@ page language="java"  import="util.SpringUtil"%>
<%@ page language="java"  import="ums.service.FunctionService"%>
<%@ page language="java"  import="java.util.List"%>
<%@ page language="java"  import="ums.model.Function"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>菜单列表</title>
<link rel="stylesheet" type="text/css" href="<%=contextPath %>/library/css/style.css"/>
</head>
<body>
<%FunctionService functionService = FunctionService.getInstance();
	List functionList = functionService.getAllFunction();
%>
功能管理>>功能列表
<br>
<table width="100%">
	<tr align="right">
		<td>
			<a href="<%=contextPath%>/framework/ums/function/functionCreate.jsp">新建功能</a>
		<td>
	</tr>
</table>
<center>
<table class="dataintable">
<tr>
	<th>功能名称</th>
	<th>URL</th>
	<th>操作</th>
</tr>
	<%for(int i = 0 ; i < functionList.size(); i++) {
		Function function =(Function)functionList.get(i);
	%>
		<tr class="function">
			<td>
				<%=function.getFunctionName()%>
			</td>
			<td>
				<%=function.getUrl()%>
			</td>
			<td>
				<a href="<%=contextPath%>/framework/ums/function/functionEdit.jsp?functionId=<%=function.getFunctionId()%>">编辑</a>
				<a href="javascript:void(0)" onclick="deleteFunction('<%=function.getFunctionId()%>',this)">删除</a>
			</td>
		</tr>
	<%}%>
</table>
</center>
<script src="<%=contextPath%>/library/js/jquery/jquery-1.3.2.js" type="text/javascript"></script>
<script type="text/javascript">
function deleteFunction(functionId,self) {
	if (!confirm("确认要删除？")) {
		return;
	}
	var url = '<%=contextPath%>/functionDeleteServlet';
	var data = {
		functionId:functionId
	}
	$.post(url,data,function(json){
		var result = json.result;
		if(result) {
			$(self).parents("tr").remove();
			$("#result").html("删除成功");
		}
		else {
			$("#result").html("删除失败");
		}
	},"json");
}
</script>
</body>
</html>