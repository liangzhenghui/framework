<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%String contextPath = request.getContextPath();%>
<%@ page language="java"  import="util.SpringUtil"%>
<%@ page language="java"  import="ums.service.MenuService"%>
<%@ page language="java"  import="ums.service.FunctionService"%>
<%@ page language="java"  import="java.util.List"%>
<%@ page language="java"  import="ums.model.Menu"%>
<%@ page language="java"  import="ums.model.Function"%>
<%MenuService menuService = MenuService.getInstance();
	List menuList = menuService.getAllMenu();
	FunctionService functionService = FunctionService.getInstance();
	List functionList = functionService.getAllFunction();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>功能管理</title>
</head>
<body>
<center>
	<h1>功能管理demo</h1>
	<a href="<%=contextPath%>/framework/ums/function/functionList.jsp">功能列表</a>
	<br>
	<a href="<%=contextPath%>/framework/ums/function/functionCreate.jsp">功能创建</a>
	<br>
</center>
</body>
<script language=javascript src="<%=contextPath%>/library/js/jquery/jquery-1.3.2.js"></script>
</html>