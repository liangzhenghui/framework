<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page language="java"  import="util.SpringUtil"%>
<%@ page language="java"  import="ums.service.MenuService"%>
<%@ page language="java"  import="java.util.List"%>
<%@ page language="java"  import="ums.service.FunctionService"%>
<%@ page language="java"  import="ums.model.Menu"%>
<%@ page language="java"  import="ums.model.Function"%>
<%@ page language="java"  import="org.apache.commons.lang.StringUtils"%>
<%String contextPath = request.getContextPath();%>
<%
String menuId = request.getParameter("menuId");
MenuService menuService = MenuService.getInstance();
Menu menu = menuService.getMenuById(menuId);
List menuList = menuService.getAllMenu();
FunctionService functionService = FunctionService.getInstance();
List functionList = functionService.getAllFunction();
String functionId = menu.getFunctionId();
Function function = null;
if(StringUtils.isNotBlank(functionId)){
	function = functionService.getFunctionById(functionId);
}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=contextPath %>/library/css/style.css"/>
<title>菜单编辑</title>
</head>
<body>
	菜单管理>>编辑菜单
	<center>
		<form id="formId" action="">
			<label>父级菜单</label>
		<!-- 获取父亲菜单的名字 -->
		<select id="parentMenu" name = "parentMenu">
		<% 
			String parentMenu = menu.getParentMenu();
			String parentMenuName = null;	
			if(StringUtils.isNotBlank(parentMenu)) {
				Menu parent =menuService.getMenuById(parentMenu);
				if(parent != null) {
					parentMenuName = parent.getMenuName();%>
				<option value="<%=parentMenu%>" selected="selected"><%=parentMenuName%></option>
				<option value="">无上级菜单</option>
				 <%for(int i = 0 ; i < menuList.size(); i++) {
					Menu m =(Menu)menuList.get(i);
					if(!m.getMenuId().equals(menu.getParentMenu())&&!m.getMenuId().equals(menu.getMenuId())) {%>
					  <option value ="<%=m.getMenuId()%>"><%=m.getMenuName()%></option>
					<%}
				   }
				}
			}
			else {%>
				<option value="" selected="selected">无上级菜单</option>
				<% for(int i = 0 ; i < menuList.size(); i++) {
					Menu m =(Menu)menuList.get(i);%>
					  <option value ="<%=m.getMenuId()%>"><%=m.getMenuName()%></option>
				<%}
			}
		%>
		</select>
		<label>引用功能</label>
		<select id="functionId" name="functionId">
		<%if(function != null) {%>
			<option value="<%=function.getFunctionId()%>" selected="selected"><%=function.getFunctionName()%></option>
				<%for(int i = 0 ; i < functionList.size(); i++) {
					Function f =(Function)functionList.get(i);
					if(f != null&&!functionId.equals(f.getFunctionId()) ) {%>
						 <option value ="<%=f.getFunctionId()%>"><%=f.getFunctionName()%></option>
					<%}%>
				<%}%>
				<option value=""></option>
			<%}else{%>
				<option value="" selected></option>
			<%
				for(int i = 0 ; i < functionList.size(); i++) {
				Function f =(Function)functionList.get(i);
			%>
			 <option value ="<%=f.getFunctionId()%>"><%=f.getFunctionName()%></option>
			<%} 
		}%>
		</select>
		<!--<label>是否发布</label><input type="radio">-->
		<label>菜单名称</label><input type="text" id="menuName" name = "menuName" value="<%=menu.getMenuName()%>">
		<input type="hidden" id="menuId" name="menuId" value="<%=menu.getMenuId()%>">
		<input type="button" id="editMenuBtn" value="编辑菜单">
		</form>
		<div id="result"></div>
	</center>
	<script language=javascript src="<%=contextPath%>/library/js/jquery/jquery-1.3.2.js"></script>
	<script type="text/javascript">
		$(function() {
			$("#editMenuBtn").click(function() {
				editMenu();
			});
		});
	
		function editMenu() {
			var menuName = $("#menuName").val();
			var parentMenu = $("#parentMenu option:selected").val();
			var functionId = $("#functionId option:selected").val();[]
			var menuId = $("#menuId").val();
			var data =  {
				menuId:menuId,
				parentMenu:parentMenu,
				functionId:functionId,
				menuName:menuName
			}
			var url = '<%=contextPath%>/menuEditServlet';
			$.post(url,data,function(json){
				var result = json.result;
				if(result) {
					$("#result").html("编辑菜单成功");
				}
				else {
					$("#result").html("编辑菜单失败");
				}
			},"json");
		}
	</script>
</body>
</html>