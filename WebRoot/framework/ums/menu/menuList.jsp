<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%String contextPath = request.getContextPath();%>
<%@ page language="java"  import="util.SpringUtil"%>
<%@ page language="java"  import="ums.service.MenuService"%>
<%@ page language="java"  import="java.util.List"%>
<%@ page language="java"  import="ums.model.Menu"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>菜单列表</title>
  	<link rel="stylesheet" type="text/css" href="<%=contextPath %>/library/css/style.css"/>
</head>
<body>
<%MenuService menuService = MenuService.getInstance();
	List menuList = menuService.getAllMenu();
%>
菜单管理>>菜单列表
<br>
<table width="100%">
	<tr align="right">
		<td>
			<a href="<%=contextPath%>/framework/ums/menu/menuCreate.jsp">新建菜单</a>
		<td>
	</tr>
</table>
<center>
<table class="dataintable">
<tr>
	<th>菜单名称</th>
	<th>操作</th>
</tr>
	<%for(int i = 0 ; i < menuList.size(); i++) {
		Menu menu =(Menu)menuList.get(i);
	%>
		<tr class="menu">
			<td>
				<%=menu.getMenuName()%>
			</td>
			<td>
				<a href="<%=contextPath%>/framework/ums/menu/menuEdit.jsp?menuId=<%=menu.getMenuId()%>">编辑</a>
				<a href="javascript:void(0)" onclick="deleteMenu('<%=menu.getMenuId()%>',this)">删除</a>
			</td>
		</tr>
	<%}%>
</table>
<div id="result">
	
</div>
</center>
<script src="<%=contextPath%>/library/js/jquery/jquery-1.3.2.js" type="text/javascript"></script>
<script type="text/javascript">
function deleteMenu(menuId,self) {
	if (!confirm("确认要删除？")) {
		return;
	}
	var url = '<%=contextPath%>/menuDeleteServlet';
	var data = {
		menuId:menuId
	}
	$.post(url,data,function(json){
		var result = json.result;
		if(result) {
			$(self).parents("tr").remove();
			$("#result").html("删除成功");
		}
		else {
			$("#result").html("删除失败");
		}
	},"json");
}
</script>
</body>
</html>