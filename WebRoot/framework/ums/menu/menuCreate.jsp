<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%String contextPath = request.getContextPath();%>
<%@ page language="java"  import="util.SpringUtil"%>
<%@ page language="java"  import="ums.service.MenuService"%>
<%@ page language="java"  import="ums.service.FunctionService"%>
<%@ page language="java"  import="java.util.List"%>
<%@ page language="java"  import="ums.model.Menu"%>
<%@ page language="java"  import="ums.model.Function"%>
<%MenuService menuService = MenuService.getInstance();
	List menuList = menuService.getAllMenu();
	FunctionService functionService = (FunctionService)SpringUtil.getBean("functionService");
	List functionList = functionService.getAllFunction();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  	<link rel="stylesheet" type="text/css" href="<%=contextPath %>/library/css/style.css"/>
<title>选择父亲菜单</title>
</head>
<body>
菜单管理>>新建菜单
<center>
	<form action="">
		<label>父级菜单</label>
		<select id="parentMenu" name="parentMenu">
		<%
		int menuSize = 	menuList.size();
		%>
		 <option value ="">无上级菜单</option>
		<%for(int i = 0 ; i < menuSize; i++) {
			Menu menu =(Menu)menuList.get(i);
		%>
		  <option value ="<%=menu.getMenuId()%>"><%=menu.getMenuName()%></option>
		<%}%>
		</select>
		<label>引用功能</label>
		<select id="functionId" name = "functionId">
			<option value="" selected="selected"></option>
			<%for(int i = 0 ; i < functionList.size(); i++) {
			Function function =(Function)functionList.get(i);
		%>
			<%if(function != null) {%>
			 <option value ="<%=function.getFunctionId()%>"><%=function.getFunctionName()%></option>
			<%}%>
		<%}%>
		</select>
		<!--<label>是否发布</label><input type="radio">-->
		<label>菜单名称</label>
		<input type="text" id="menuName" name="menuName">
		<input type = "button" id="createMenuBtn" value="创建菜单">
		<div id="result"></div>
	</form>
</center>
<script language=javascript src="<%=contextPath%>/library/js/jquery/jquery-1.3.2.js"></script>
<script type="text/javascript">
$(function() {
	$("#createMenuBtn").click(function() {
		createMenu();
	});
});
function createMenu() {
	var menuName = $("#menuName").val();
	if(menuName.length ==0) {
		$("#result").html("菜单名不能为空");
		return;
	}
	var parentMenu = $("#parentMenu option:selected").val();
	var functionId = $("#functionId option:selected").val();
	var menuName = $("#menuName").val();
	var url = '<%=contextPath%>/menuCreateServlet';
	var data = {
		parentMenu:parentMenu,
		functionId:functionId,
		menuName:menuName
	}
	$.post(url,data,function(json){
		var result =json.result;
		if(result) {
			$("#result").html("创建菜单成功");
		}
		else {
			$("#result").html("创建菜单失败");
		}
	},"json");
}
</script>
</body>
</html>