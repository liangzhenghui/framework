<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%String contextPath = request.getContextPath();%>
<%@ page language="java"  import="util.SpringUtil"%>
<%@ page language="java"  import="ums.model.User"%>
<%@ page language="java"  import= "ums.service.UserService"%>
<%
String id = (String)request.getParameter("id");
UserService userService = UserService.getInstance();
User user = userService.getUserById(id);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=contextPath %>/library/css/style.css"/>
<title>用户编辑</title>
</head>
<body>
用户管理>>用户编辑
	<center>
		<form id="formId" action="<%=contextPath%>/userEditServlet" method="post">
			<label>账号</label>
			<input type="text" id="userid" name="userid" value="<%=user.getUserid()%>">
			<input type="hidden" id="id" name="id" value="<%=user.getId()%>">
			<label>姓名</label>
			<input type="text" id="username" name="username" value="<%=user.getUsername()%>">
			<input type="button" id="editUserBtn" value="编辑用户">
		</form>
		<div id="result"></div>
	</center>
	<script language=javascript src="<%=contextPath%>/library/js/jquery/jquery-1.3.2.js"></script>
	<script type="text/javascript">
		$(function() {
			$("#editUserBtn").click(function() {
				editUser();
			});
		});
	
		function editUser() {
			var id = $("#id").val();
			var username = $("#username").val();
			username = encodeURIComponent(username); 
			var userid = $("#userid").val();
			var url = '<%=contextPath%>/userEditServlet?id='+id+'&username='+username+'&userid='+userid;
			$.post(url,function(data){
				var result = eval(data);
				if(result) {
					$("#result").html("编辑用户成功");
				}
				else {
					$("#result").html("编辑用户败");
				}
			});
		}
	</script>
</body>
</html>