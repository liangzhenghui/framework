<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="service.*" pageEncoding="UTF-8"%>
<%@ page language="java"  import="java.util.List"%>
<%@ page language="java"  import="ums.service.UserService"%>
<%@ page language="java"  import="ums.model.User"%>
<%@ page language="java"  import="util.SpringUtil"%>
<%String contextPath = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<%=contextPath %>/library/css/style.css"/>
<title>展示用户demo</title>
</head>
<body>
<%
UserService userService = UserService.getInstance();
List list =userService.getUsers();
int size = list.size();
%>
用户管理>>用户列表
<br>
<table width="100%">
	<tr align="right">
		<td>
			<a href="<%=contextPath%>/framework/ums/user/userCreate.jsp">新建用户</a>
		<td>
	</tr>
</table>
<center>
<table class="dataintable">
	<thead>
	<tr>
		<th>账号</th>
		<th>姓名</th>
		<th>操作</th>
	</tr>
	</thead>
	<tbody>
	<%for(int i = 0 ; i< size;i++) {
		User user=(User)list.get(i);
	%>
	<tr>
	<td><%=user.getUserid()%></td>
	<td><%=user.getUsername()%></td>
	<td>
		<a href="<%=contextPath%>/framework/ums/user/userEdit.jsp?id=<%=user.getId()%>">编辑</a>
		<a href="javascript:void(0)>" onclick="deleteUser('<%=user.getId()%>',this)">删除</a>
	</td>
</tr>
<%}%>
</tbody>
</table>
<div id="result"></div>
<script src="<%=contextPath%>/library/js/jquery/jquery-1.3.2.js" type="text/javascript"></script>
<script type="text/javascript">
function deleteUser(id,self) {
	if (!confirm("确认要删除？")) {
		return;
	}
	var url = '<%=contextPath%>/userDeleteServlet';
	var data = {
		id:id
	}
	$.post(url,data,function(json){
		var result = json.result;
		if(result) {
			$(self).parents("tr").remove();
			$("#result").html("删除成功");
		}
		else {
			$("#result").html("删除失败");
		}
	},"json");
}
</script>
</center>
</body>
</html>