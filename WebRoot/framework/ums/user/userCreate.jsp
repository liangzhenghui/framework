<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%String contextPath = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" type="text/css" href="<%=contextPath %>/library/css/style.css"/>
<title>创建用户</title>
</head>
<body>
用户管理>>创建用户
	<center>
		<form id="formId" action="">
			<label>账号</label>
			<input type="text" id="userid" name="userid" value=""/>
			<label>姓名</label>
			<input type="text" id="username" name="username" value=""/>
			<input type="button" id="createUserBtn" value="创建用户"/>
		</form>
		<div id="result"></div>
	</center>
</body>
<script language=javascript src="<%=contextPath%>/library/js/jquery/jquery-1.3.2.js"></script>
<script type="text/javascript">
$(function() {
	$("#createUserBtn").click(function() {
		var userid = $("#userid").val().trim();
		var username = $("#username").val().trim();
		userid = encodeURIComponent(userid);
		username = encodeURIComponent(username);
		if(userid == "" || username == "") {
			$("#result").html("账号或用户名不能为空");
			return;
		}
		//要先判断userid是否已经存在
		createUser(userid, username);
	});
});

function useridisExists(userid) {
}

function createUser(userid, username) {
	var url = '<%=contextPath%>/useridisExistsServlet';
	var data = {
		userid : userid
	};
	$.post(url,data,function(json){
		var result = json.result;
		if(result) {
			$("#result").html("账号已经存在,请重新换个账号");
			return;
		}
		else {
			var url ="";
			url = '<%=contextPath%>/userCreateServlet?userid='+userid+'&username='+username;
			$.post(url,function(data){
				var result = eval(data);
				if(result) {
					$("#result").html("创建用户成功");
				}
				else {
					$("#result").html("创建用户失败");
				}
			});
		}
	},"json");
	
}
</script>
</html>