<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%String contextPath = request.getContextPath();%>
<%@ page language="java"  import="util.SpringUtil"%>
<%@ page language="java"  import="ums.model.Role"%>
<%@ page language="java"  import="ums.service.RoleService"%>
<%@ page language="java"  import="util.SpringUtil"%>
<%@ page language="java"  import="ums.model.User"%>
<%@ page language="java"  import="ums.service.UserService"%>
<%@ page language="java"  import="java.util.List"%>
<%RoleService roleService = RoleService.getInstance();
List roleList = roleService.getAllRoles();

UserService userService = UserService.getInstance();
List userList = userService.getUsers();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<%=contextPath%>/library/js/zTree/zTreeStyle.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<%=contextPath %>/library/css/style.css"/>
<title>角色赋予用户</title>
</head>
<body>
角色管理>>分配角色
<center>
	<form action="">
		<table>
			<tr>
				<td>
					<label>角色名称：</label>
					<select id="roleName" name = "roleName">
						<%for(int i = 0;i <roleList.size(); i++) {
							Role role = (Role)roleList.get(i);
						%>
						<option value="<%=role.getRoleId()%>"><%=role.getRoleName() %></option>
						<%} %>
					</select>
				</td>
				<td>
					<label>用户名称：</label>
					<select id="userName" name = "userName">
						<%for(int i = 0;i <userList.size(); i++) {
							User user = (User)userList.get(i);
						%>
						<option value="<%=user.getId()%>"><%=user.getUsername() %></option>
						<%} %>
					</select>
				</td>
				<td>
					<input type="button" id="roleGrantUserBtn" value="确定">
				</td>
			</tr>
			<tr>
				<td id="result"></td>
			</tr>
		</table>
	</form>
</center>
<script src="<%=contextPath%>/library/js/jquery/jquery-1.3.2.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(function() {
		$("#roleGrantUserBtn").click(function(){
			var roleId = $("#roleName option:selected").val();
			var userId = $("#userName option:selected").val();
			var url = '<%=contextPath%>/roleGrantUserServlet';
			var data = {
				roleId:roleId,
				userId:userId
			}
			$.post(url, data,function(json){
				var result = json.result;
				if(result) {
					$("#result").html("给用户赋予角色成功！");
				}
			},"json");
		});
	});
	</script>
</body>
</html>