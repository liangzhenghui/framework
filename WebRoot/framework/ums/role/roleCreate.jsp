<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%String contextPath = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=contextPath %>/library/css/style.css"/>
<link href="<%=contextPath%>/library/js/zTree/zTreeStyle.css" rel="stylesheet" />
<title>创建角色</title>
</head>
<body>
角色管理>>新建角色
<center>
	<form action="">
		<table>
			<tr>
				<td>
					<label>角色名称：</label><input id= "roleName" type="text" name="roleName">
				</td>
				<td>
					<input type="button" id="createRoleBtn" value="确定">
				</td>
			</tr>
			<tr>
				<td id="result"></td>
			</tr>
		</table>
	</form>
</center>
<script src="<%=contextPath%>/library/js/jquery/jquery-1.3.2.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(function(){
		$("#createRoleBtn").click(function() {
			createRole();
		});
	});
	function createRole() {
		var roleName = $("#roleName").val();
		if(roleName.length ==0) {
			$("#result").html("角色名不能为空");
			return;
		}
		var url = '<%=contextPath%>/roleCreateServlet';
		var data={
			roleName:roleName
		}
		$.post(url,data,function(json){
			var result = json.result;
			if(result) {
				$("#result").html("创建角色成功");
			}
			else {
				$("#result").html("创建角色失败");
			}
		},"json");
	}
	</script>
</body>
</html>