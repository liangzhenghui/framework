<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%String contextPath = request.getContextPath();%>
<%@ page language="java"  import="util.SpringUtil"%>
<%@ page language="java"  import="ums.service.RoleService"%>
<%@ page language="java"  import="java.util.List"%>
<%@ page language="java"  import="ums.model.Role"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>菜单列表</title>
<link rel="stylesheet" type="text/css" href="<%=contextPath %>/library/css/style.css"/>
<link href="<%=contextPath%>/library/js/zTree/zTreeStyle.css" rel="stylesheet" />
</head>
<body>
<%RoleService roleService = RoleService.getInstance();
	List roleList = roleService.getAllRoles();
%>
角色管理>>角色列表
<br>
<table width="100%">
	<tr align="right">
		<td>
			<a href="<%=contextPath%>/framework/ums/role/roleCreate.jsp">新建角色</a>
			<a href="<%=contextPath%>/framework/ums/menu/menuGrantRole.jsp">分配权限</a>
			<a href="<%=contextPath%>/framework/ums/role/roleGrantUser.jsp">分配角色</a>
		<td>
	</tr>
</table>
<center>
<table class="dataintable">
<tr>
	<th>角色名称</th>
	<th>权限</th>
	<th>操作</th>
</tr>
	<%for(int i = 0 ; i < roleList.size(); i++) {
		Role role =(Role)roleList.get(i);
	%>
		<tr class="function">
			<td>
				<%=role.getRoleName()%>
			</td>
			<td class="tree">
				<div>
					<ul id="<%=role.getRoleId()%>" class="ztree">
						
					</ul>
				</div>
				<input type="hidden" value="<%=role.getRoleId()%>">
			</td>
			<td>
				<a href="<%=contextPath%>/framework/ums/role/menusOfRoleEdit.jsp?roleId=<%=role.getRoleId()%>">编辑</a>
				<a href="javascript:void(0)" onclick="deleteRole('<%=role.getRoleId()%>',this)">删除</a>
			</td>
		</tr>
	<%}%>
</table>
</center>
<script src="<%=contextPath%>/library/js/jquery/jquery-1.3.2.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=contextPath%>/library/js/zTree/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="<%=contextPath%>/library/js/zTree/jquery.ztree.excheck-3.5.js"></script>
<script type="text/javascript">
var setting = {
		data: {
			key: {
				name:"name"
			},
			simpleData: {
				idKey:"id",
				pIdKey:"pId",
				rootPId:null,
				enable: true
			}
		}
	};

	var zNodes = [];
	$(function(){
		$(".tree").each(function(){
			var roleId = $(this).find("input").val();
			var url = '<%=contextPath%>/menuTreeForOneRoleServlet?roleId='+roleId;
			$.post(url,function(data){
				//将json对象转换成js识别的对象
				zNodes = eval(data);
				var treeObj = $.fn.zTree.init($('#'+roleId), setting, zNodes);
				expandFirstNode(treeObj);
			});
		});
		
	});
	
	function expandFirstNode(treeObj){
		var nodes = treeObj.getNodes();
		if (nodes.length > 0) {
			treeObj.expandNode(nodes[0], true,false,false,false);
		}
	}
	
	function deleteRole(roleId,self) {
		if (!confirm("确认要删除？")) {
			return;
		}
		var url = '<%=contextPath%>/roleDeleteServlet';
		var data = {
			roleId:roleId
		}
		$.post(url,data,function(json){
			var result = json.result;
			if(result) {
				$(self).parents("tr").remove();
				$("#result").html("删除成功");
			}
			else {
				$("#result").html("删除失败");
			}
		},"json");
	}
</script>
</body>
</html>