<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%String contextPath = request.getContextPath();%>
<%@ page language="java"  import="util.SpringUtil"%>
<%@ page language="java"  import="ums.service.DepartmentService"%>
<%@ page language="java"  import="java.util.List"%>
<%@ page language="java"  import="ums.model.Department"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>机构列表</title>
  	<link rel="stylesheet" type="text/css" href="<%=contextPath %>/library/css/style.css"/>
</head>
<body>
<%DepartmentService departmentService = DepartmentService.getInstance();
	List departmentList = departmentService.getAllDepartments();
%>
机构管理>>机构列表
<br>
<table width="100%">
	<tr align="right">
		<td>
			<a href="<%=contextPath%>/framework/ums/department/departmentTree.jsp">查看机构树</a>
			<a href="<%=contextPath%>/framework/ums/department/departmentCreate.jsp">新建机构</a>
		<td>
	</tr>
</table>
<center>
<table class="dataintable">
<tr>
	<th>机构名称</th>
	<th>操作</th>
</tr>
	<%for(int i = 0 ; i < departmentList.size(); i++) {
		Department department =(Department)departmentList.get(i);
	%>
		<tr class="department">
			<td>
				<%=department.getName()%>
			</td>
			<td>
				<a href="<%=contextPath%>/framework/ums/department/departmentEdit.jsp?id=<%=department.getId()%>">编辑</a>
				<a href="javascript:void(0)" onclick="deleteDepartment('<%=department.getId()%>',this)">删除</a>
			</td>
		</tr>
	<%}%>
</table>
<div id="result">
	
</div>
</center>
<script src="<%=contextPath%>/library/js/jquery/jquery-1.3.2.js" type="text/javascript"></script>
<script type="text/javascript">
function deleteDepartment(id,self) {
	if (!confirm("确认要删除？")) {
		return;
	}
	var url = '<%=contextPath%>/departmentDeleteServlet';
	var data = {
		id:id
	}
	$.post(url,data,function(json){
		var result = json.result;
		if(result) {
			$(self).parents("tr").remove();
			$("#result").html("删除成功");
		}
		else {
			$("#result").html("删除失败");
		}
	},"json");
}
</script>
</body>
</html>