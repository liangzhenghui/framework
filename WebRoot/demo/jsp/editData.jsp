<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page language="java"  import="org.springframework.context.*"%>
<%@ page language="java"  import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@ page language="java"  import="util.SpringUtil"%>
<%@ page language="java"  import="model.User"%>
<%String contextPath = request.getContextPath();%>
<%@ page language="java"  import= "service.UserService"%>
<%
String userId = (String)request.getParameter("userId");
UserService userService = (UserService)SpringUtil.getBean("userService");
User user = userService.getUserById(userId);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<center>
		<form id="formId" action="<%=contextPath%>/userEditServlet" method="post">
			<label>姓名</label>
			<input type="text" name="username" value="<%=user.getUsername()%>">
			<input type="hidden" name="id" value="<%=user.getId()%>">
			<input type="submit" value="编辑用户">
		</form>
	</center>
</body>
</html>