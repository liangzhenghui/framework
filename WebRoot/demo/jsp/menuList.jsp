<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%String contextPath = request.getContextPath();%>
<%@ page language="java"  import="util.SpringUtil"%>
<%@ page language="java"  import="ums.service.MenuService"%>
<%@ page language="java"  import="java.util.List"%>
<%@ page language="java"  import="ums.model.Menu"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>菜单列表</title>
</head>
<body>
<%MenuService menuService = (MenuService)SpringUtil.getBean("menuService");
	List menuList = menuService.getAllMenu();
%>
<center>
	<h1>菜单列表demo</h1>
<table>
<tr>
	<td>菜单ID</td>
	<td>菜单名称</td>
	<td>操作</td>
</tr>
	<%for(int i = 0 ; i < menuList.size(); i++) {
		Menu menu =(Menu)menuList.get(i);
	%>
		<tr class="menu">
			<td style="hidden">
				<%=menu.getMenuId()%>
			</td>
			<td>
				<%=menu.getMenuName()%>
			</td>
			<td>
				<a href="<%=contextPath%>/demo/jsp/menuEdit.jsp?menuId=<%=menu.getMenuId()%>">编辑</a>
			</td>
			<td>
				<a href="<%=contextPath%>/menuDeleteServlet?menuId=<%=menu.getMenuId()%>">删除</a>
			</td>
		</tr>
	<%}%>
</table>
</center>
</body>
</html>