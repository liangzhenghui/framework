<%@ page language="java" contentType="text/html; charset=UTF-8"
    import="service.*" pageEncoding="UTF-8"%>
<%@ page language="java"  import="java.util.List"%>
<%@ page language="java"  import="model.User"%>
<%@ page language="java"  import="org.springframework.context.*"%>
<%@ page language="java"  import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%String contextPath = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>展示数据</title>
</head>
<body>
<%ApplicationContext ac = new ClassPathXmlApplicationContext("spring/applicationContext-*.xml");
UserService userService = (UserService)ac.getBean("userService");
List list =userService.getUsers();
int size = list.size();
%>

<center>
	<a href="<%=contextPath%>/demo/jsp/InsertData.jsp">创建用户</a>
	<br>
	<a href="<%=contextPath%>/demo/jsp/InsertDataForTestTransaction.jsp">创建用户,用于测试事务是否正确执行</a>
	<br>
	<a href="<%=contextPath%>/demo/jsp/InsertDataForTestWithOutTransaction.jsp">创建用户,用于测试去掉事务执行效果</a>
</center>
<table align="center">
	<thead>
	<tr>
		<td>ID</td>
		<td>姓名</td>
	</tr>
</thead>
		<tbody>
<%for(int i = 0 ; i< size;i++) {
	User user=(User)list.get(i);
%>
<tr>
<td><%=user.getId()%></td>
<td><%=user.getUsername()%></td>
<td>
	<a href="<%=contextPath%>/demo/jsp/editData.jsp?userId=<%=user.getId()%>">编辑</a>
</td>
<td>
	<a href="<%=contextPath%>/userDeleteServlet?userId=<%=user.getId()%>">删除</a>
</td>
</tr>
<%}%>
</tbody>
</table>
</body>
</html>