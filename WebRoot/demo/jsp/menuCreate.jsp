<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%String contextPath = request.getContextPath();%>
<%@ page language="java"  import="util.SpringUtil"%>
<%@ page language="java"  import="ums.service.MenuService"%>
<%@ page language="java"  import="ums.service.FunctionService"%>
<%@ page language="java"  import="java.util.List"%>
<%@ page language="java"  import="ums.model.Menu"%>
<%@ page language="java"  import="ums.model.Function"%>
<%MenuService menuService = (MenuService)SpringUtil.getBean("menuService");
	List menuList = menuService.getAllMenu();
	FunctionService functionService = (FunctionService)SpringUtil.getBean("functionService");
	List functionList = functionService.getAllFunction();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>选择父亲菜单</title>
</head>
<body>
<center>
	<h1>创建菜单demo</h1>
	<form action="<%=contextPath%>/menuCreateServlet" method="post">
		<label>父级菜单</label>
		
		<select name = "parentMenu">
		<%for(int i = 0 ; i < menuList.size(); i++) {
			Menu menu =(Menu)menuList.get(i);
		%>
		  <option value ="<%=menu.getMenuId()%>"><%=menu.getMenuName()%></option>
		<%}%>
		</select>
		<label>引用功能</label>
		<select name = "functionId">
			<%for(int i = 0 ; i < functionList.size(); i++) {
			Function function =(Function)functionList.get(i);
		%>
			<%if(function != null) {%>
			 <option value ="<%=function.getFunctionId()%>"><%=function.getFunctionName()%></option>
			<%}%>
		<%}%>
		</select>
		<!--<label>是否发布</label><input type="radio">-->
		<label>菜单名称</label><input type="text" name = "menuName">
		<input type = "submit" value="创建菜单">
	</form>
</center>
</body>
<script language=javascript src="<%=contextPath%>/library/js/jquery/jquery-1.3.2.js"></script>
</html>