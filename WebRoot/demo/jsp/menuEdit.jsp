<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page language="java"  import="util.SpringUtil"%>
<%@ page language="java"  import="ums.service.MenuService"%>
<%@ page language="java"  import="java.util.List"%>
<%@ page language="java"  import="ums.service.FunctionService"%>
<%@ page language="java"  import="ums.model.Menu"%>
<%@ page language="java"  import="ums.model.Function"%>
<%@ page language="java"  import="org.apache.commons.lang.StringUtils"%>
<%String contextPath = request.getContextPath();%>
<%
String menuId = request.getParameter("menuId");
MenuService menuService = (MenuService)SpringUtil.getBean("menuService");
Menu menu = menuService.getMenuById(menuId);
List menuList = menuService.getAllMenu();
FunctionService functionService = (FunctionService)SpringUtil.getBean("functionService");
List functionList = functionService.getAllFunction();
String functionId = menu.getFunctionId();
Function function = null;
if(StringUtils.isNotBlank(functionId)){
	function = functionService.getFunctionById(functionId);
}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>菜单编辑</title>
</head>
<body>
	<center>
		<h1>菜单编辑demo</h1>
		<form id="formId" action="<%=contextPath%>/menuEditServlet" method="post">
			<label>父级菜单</label>
		
		<select name = "parentMenu">
		<!-- 获取父亲菜单的名字 -->
		<% 
			Menu parent =menuService.getMenuById(menu.getParentMenu());
			String parentMenuName = null;	
			if(parent != null) {
				parentMenuName = parent.getMenuName();
			}
		%>
			<option value="<%=menu.getParentMenu()%>" selected="selected"><%=parentMenuName%></option>
			<%for(int i = 0 ; i < menuList.size(); i++) {
				Menu m =(Menu)menuList.get(i);
				if(!m.getMenuId().equals(menu.getParentMenu())) {%>
				  <option value ="<%=m.getMenuId()%>"><%=m.getMenuName()%></option>
				<%}
			}%>
		</select>
		<label>引用功能</label>
		<select name="functionId">
		<%if(function != null) {%>
			<option value="<%=function.getFunctionId()%>" selected="selected"><%=function.getFunctionName()%></option>
				<%for(int i = 0 ; i < functionList.size(); i++) {
					Function f =(Function)functionList.get(i);
					if(f != null&&!functionId.equals(f.getFunctionId()) ) {%>
						 <option value ="<%=f.getFunctionId()%>"><%=f.getFunctionName()%></option>
					<%}%>
				<%}%>
			<%}else{%>
			<%
				for(int i = 0 ; i < functionList.size(); i++) {
				Function f =(Function)functionList.get(i);
			%>
			 <option value ="<%=f.getFunctionId()%>"><%=f.getFunctionName()%></option>
			<%} 
		}%>
		</select>
		<!--<label>是否发布</label><input type="radio">-->
		<label>菜单名称</label><input type="text" name = "menuName" value="<%=menu.getMenuName()%>">
		<input type="hidden" name="menuId" value="<%=menu.getMenuId()%>">
		<input type="submit" value="编辑菜单">
		</form>
	</center>
</body>
</html>