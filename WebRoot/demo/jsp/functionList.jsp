<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%String contextPath = request.getContextPath();%>
<%@ page language="java"  import="util.SpringUtil"%>
<%@ page language="java"  import="ums.service.FunctionService"%>
<%@ page language="java"  import="java.util.List"%>
<%@ page language="java"  import="ums.model.Function"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>菜单列表</title>
</head>
<body>
<%FunctionService functionService = (FunctionService)SpringUtil.getBean("functionService");
	List functionList = functionService.getAllFunction();
%>
<center>
	<h1>功能列表展示demo</h1>
<table>
<tr>
	<td>功能ID</td>
	<td>功能名称</td>
	<td>URL</td>
	<td>操作</td>
</tr>
	<%for(int i = 0 ; i < functionList.size(); i++) {
		Function function =(Function)functionList.get(i);
	%>
		<tr class="function">
			<td>
				<%=function.getFunctionId()%>
			</td>
			<td>
				<%=function.getFunctionName()%>
			</td>
			<td>
				<%=function.getUrl()%>
			</td>
			<td>
				<a href="<%=contextPath%>/demo/jsp/functionEdit.jsp?functionId=<%=function.getFunctionId()%>">编辑</a>
			</td>
			<td>
				<a href="<%=contextPath%>/functionDeleteServlet?functionId=<%=function.getFunctionId()%>">删除</a>
			</td>
		</tr>
	<%}%>
</table>
</center>
</body>
</html>