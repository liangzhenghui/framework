<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page language="java"  import="util.SpringUtil"%>
<%@ page language="java"  import="ums.service.FunctionService"%>
<%@ page language="java"  import="java.util.List"%>
<%@ page language="java"  import="ums.model.Function"%>
<%String contextPath = request.getContextPath();%>
<%
String functionId = (String)request.getParameter("functionId");
FunctionService functionService = (FunctionService)SpringUtil.getBean("functionService");
Function function = functionService.getFunctionById(functionId);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>功能编辑</title>
</head>
<body>
	<center>
		<h1>功能编辑demo</h1>
		<form action="<%=contextPath%>/functionEditServlet" method="post">
		<!--<label>是否发布</label><input type="radio">-->
		<label>功能名称</label><input type="text" name = "functionName" value="<%=function.getFunctionName()%>">
		<label>功能URL</label><input style= "width:400px" type="text" name = "url" value="<%=function.getUrl()%>">
		<input type="hidden" name="functionId" value="<%=function.getFunctionId() %>">
		<input type="submit" value="编辑功能">
		</form>
	</center>
</body>
</html>