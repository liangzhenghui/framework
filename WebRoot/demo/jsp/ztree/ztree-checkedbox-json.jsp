<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%String contextPath = request.getContextPath();%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<%=contextPath%>/library/js/zTree/zTreeStyle.css" rel="stylesheet" />
<title>zTree use json Data</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div>
				<ul id="treeDemo" class="ztree">
					
				</ul>
			</div>
		</div>	
	</div>
	<script src="<%=contextPath%>/library/js/jquery/jquery-1.3.2.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=contextPath%>/library/js/zTree/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="<%=contextPath%>/library/js/zTree/jquery.ztree.excheck-3.5.js"></script>
	<script type="text/javascript">
	var setting = {
			check: {
				enable: true,
				chkStyle: "checkbox",
				chkboxType: {"Y":"ps","N":"s"},
				open:true
			},
			data: {
				key: {
					name:"name"
				},
				simpleData: {
					idKey:"id",
					pIdKey:"pId",
					rootPId:null,
					enable: true
				}
			}
		};

		var zNodes =[];
		zNodes =[{ id:1, pId:0, name:"can check 1", open:true},
		{ id:11, pId:1, name:"can check 1-1", open:true},
		{ id:111, pId:11, name:"can check 1-1-1"},
		{ id:112, pId:11, name:"can check 1-1-2"},
		{ id:12, pId:1, name:"can check 1-2", open:true},
		{ id:121, pId:12, name:"can check 1-2-1"},
		{ id:122, pId:12, name:"can check 1-2-2"},
		{ id:2, pId:0, name:"can check 2", checked:true, open:true},
		{ id:21, pId:2, name:"can check 2-1"},
		{ id:22, pId:2, name:"can check 2-2", open:true},
		{ id:221, pId:22, name:"can check 2-2-1", checked:true},
		{ id:222, pId:22, name:"can check 2-2-2"},
		{ id:23, pId:2, name:"can check 2-3"}];
		$(function(){
			//var url = '<%=contextPath%>group/group-json-tree.action';
			//$.post(url,function(data){
				//zNodes = data.groups;
				//var treeObj = $.fn.zTree.init($("#treeDemo"), setting, zNodes);
				//expandFirstNode(treeObj);
			//});
			var treeObj = $.fn.zTree.init($("#treeDemo"), setting, zNodes);
			expandFirstNode(treeObj);
		});
		
		function expandFirstNode(treeObj){
			var nodes = treeObj.getNodes();
			if (nodes.length > 0) {
				treeObj.expandNode(nodes[0], true,false,false,false);
			}
		}
	</script>
</body>
</html>