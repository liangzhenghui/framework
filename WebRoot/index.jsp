<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page language="java"  import="ums.model.User"%>
<%@ page language="java"  import="ums.model.Role"%>
<%@ page language="java"  import="ums.model.Menu"%>
<%@ page language="java"  import="ums.model.Function"%>
<%@ page language="java"  import="ums.service.RoleService"%>
<%@ page language="java"  import="ums.service.MenuService"%>
<%@ page language="java"  import="ums.service.FunctionService"%>
<%@ page language="java"  import="util.SpringUtil"%>
<%@ page language="java"  import="java.util.List"%>
<%String contextPath = request.getContextPath();%>
<%HttpSession httpSession = request.getSession(); 
User user = (User)httpSession.getAttribute("user");
String username = user.getUsername();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<%=contextPath%>/library/js/zTree/zTreeStyle.css" rel="stylesheet" />
<title>欢迎来到liangzhenghui的架构之路</title>
<style type="text/css">
body { font-family: Verdana; font-size: 12px; line-height: 1.5; }
#menu { border: 1px solid #CCC; height:26px; background: #eee;}
#menu ul { list-style: none; margin: 0px; padding: 0px; }
#menu ul li { float:left; padding: 0px 8px; height: 26px; line-height: 26px; }
#menu ul li a {  text-decoration: none;display:block; padding: 0px 8px; height: 26px; line-height: 26px; float:left;}
#menu ul li a:hover { background:#333; color:#fff;}
#menu ul li.username {
	float:right;
}
#menu ul li.username a {
	float:right;
}

#header{margin:0 auto;width:800px}
#bigContainer{width:100%;height:600px}
#container{margin:0 auto;width:1000px;height:100%}
#rightContent{height:100%}
</style>
</head>
<body>
<div id="menu">
	<div id="header">
		<ul>
		<li><a href="#">首页</a></li>
		<li class="username"><%=username %>,您好|<a href="#">退出</a></li>
		</ul>
	</div>
</div>
<div id="bigContainer">
	<div id="container">
	<table width="100%" height="100%">
	<tr>
		<td valign="top" width="150px">
			<div id="verticalMenu">
					<ul id="menuTree" class="ztree">
						
					</ul>
			</div>
			<td>
			<td height="100%">
				<div id="rightContent">
					<iframe id="iframe" src="" width="100%" height="100%" frameborder="0" scrolling="auto"></iframe>
				</div>
			<td>
		</tr>
		</table>
	</div>
</div>
<script src="<%=contextPath%>/library/js/jquery/jquery-1.3.2.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=contextPath%>/library/js/zTree/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript" src="<%=contextPath%>/library/js/zTree/jquery.ztree.excheck-3.5.js"></script>
	<script type="text/javascript">
	var setting = {
			data: {
				key: {
					name:"name"
				},
				simpleData: {
					idKey:"id",
					pIdKey:"pId",
					rootPId:null,
					enable: true
				}
			},
			callback: {
				onClick:zTreeOnClick
			}
		};
		function zTreeOnClick(event, treeId, treeNode) {
			if(treeNode.functionUrl=="#"){
				return;
			}
			else{
				$("#iframe").attr("src",'<%=contextPath%>'+treeNode.functionUrl);
			}
		};

		var zNodes = [];
		$(function(){
			var url = '<%=contextPath%>/systemMenuTreeServlet';
			$.post(url,function(data){
				//将json对象转换成js识别的对象
				zNodes = eval(data);
				var treeObj = $.fn.zTree.init($("#menuTree"), setting, zNodes);
				expandFirstNode(treeObj);
			});
		});
		
		function expandFirstNode(treeObj){
			var nodes = treeObj.getNodes();
			if (nodes.length > 0) {
				treeObj.expandNode(nodes[0], true,false,false,false);
			}
		}
	</script>
</body>
</html>