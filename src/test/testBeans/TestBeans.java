package testBeans;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import service.HelloJdbcService;

import dao.JdbcService;

public class TestBeans {
	@Test
	public void testJdbcService() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("spring/applicationContext-service.xml");
		JdbcService jdbcService = (JdbcService)ac.getBean("jdbcService");
		assertNotNull(null,jdbcService);
	}
	
	@Test
	public void testDataSource() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("spring/applicationContext-datasource.xml");
		JdbcTemplate jdbcTemplate = (JdbcTemplate)ac.getBean("jdbcTemplate");
		assertNotNull(null,jdbcTemplate);
	}
	
	@Test
	public void testHelloWorldService() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("spring/applicationContext-service.xml");
		HelloJdbcService helloJdbcService = (HelloJdbcService)ac.getBean("helloJdbcService");
		assertNotNull(null,helloJdbcService);
		assertEquals("HelloWorld",helloJdbcService.getName());
		System.out.println(helloJdbcService.getName());
	}
}
