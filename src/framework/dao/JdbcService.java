package dao;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

public class JdbcService{
	 
	private JdbcTemplate jdbcTemplate;
	
	
	public int update(String sql, Object[] args, int[] argTypes) {
		return jdbcTemplate.update(sql, args, argTypes);
	}
	
	public List queryForList(String sql, Object[] args,RowMapper rowMapper) {
		return jdbcTemplate.query(sql, args, rowMapper);
	}
	
	
	public List queryForList(String sql, Object[] args) {
		return jdbcTemplate.queryForList(sql, args);
	}
	
	public Object queryForObject(String sql, RowMapper rowMapper) {
		return jdbcTemplate.queryForObject(sql, rowMapper);
	}
	//返回记录的条数
	public int count(String sql, Object[] args) {
		return jdbcTemplate.queryForInt(sql, args);
	}
	
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		System.out.println("jdbcTemplate is set!");
		this.jdbcTemplate = jdbcTemplate;
	}
}
