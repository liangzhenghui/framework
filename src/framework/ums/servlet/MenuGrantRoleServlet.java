package ums.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import ums.service.MenuService;
import util.SpringUtil;

/**
 * @author liangzhenghui
 * @date Aug 21, 2013    9:56:04 AM
 */
public class MenuGrantRoleServlet extends HttpServlet {

	private Boolean result = false;
	private static final long serialVersionUID = 9100962471137094302L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		JSONObject json = new JSONObject();
		String[] menuIds = req.getParameterValues("menuId");
		String roleId = req.getParameter("roleId");
		MenuService menuService = MenuService.getInstance();
		menuService.grantMenuToRole(roleId, menuIds);
		result = true;
		json.put("result", result);
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = resp.getWriter();
	    out.println(json.toJSONString());
	}

}
