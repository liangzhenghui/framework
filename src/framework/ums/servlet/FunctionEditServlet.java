package ums.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ums.service.FunctionService;
import util.SpringUtil;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * @author liangzhenghui
 * @date Aug 21, 2013    11:35:54 AM
 */
public class FunctionEditServlet extends HttpServlet {

	private static final long serialVersionUID = 6698023905718559884L;
	private Boolean result = false;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		JSONObject json = new JSONObject();
		String functionId = req.getParameter("functionId");
		String functionName = req.getParameter("functionName");
		String url = req.getParameter("url");
		FunctionService functionService = FunctionService.getInstance();
		functionService.editFunction(functionId, functionName, url);
		result = true;
		json.put("result", result);
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = resp.getWriter();
	    out.println(json.toJSONString());
	}

}
