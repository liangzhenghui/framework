package ums.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import ums.service.FunctionService;
import ums.service.RoleService;
import util.SpringUtil;

/**
 * @author liangzhenghui
 * @date Aug 24, 2013    9:46:43 PM
 * 创建角色
 */
public class RoleCreateServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	Boolean result = false;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		JSONObject json = new JSONObject();
		String roleName = req.getParameter("roleName");
		RoleService roleService = RoleService.getInstance();
		roleService.createRole(roleName);
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = resp.getWriter();
		result = true;
		json.put("result", result);
	    out.println(json.toJSONString());
	}

}
