package ums.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ums.model.MenuTree;
import ums.model.User;
import ums.service.MenuService;
import ums.service.RoleService;
import util.SpringUtil;

import com.alibaba.fastjson.JSON;

/**
 * @author liangzhenghui
 * @date Sep 13, 2013    7:57:39 PM
 */
public class MenuTreeForOneRoleEditServlet extends HttpServlet {
	
	private static final long serialVersionUID = 4315110956905406813L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		MenuService menuService = MenuService.getInstance();
		String roleId = req.getParameter("roleId");
		List<MenuTree> menuTreeListOfOneRole = null;
		List<MenuTree> menuTreeOfAll = null;
		if(StringUtils.isNotBlank(roleId)) {
			menuTreeListOfOneRole = menuService.getMenuTreeByRoleId(roleId);
		}
		//取得所有的菜单,并且将菜单都放到List<MenuTree>中返回来
		menuTreeOfAll = menuService.getMenuTree();
		for(MenuTree menuTree : menuTreeOfAll) {
			for(MenuTree menuTreeOfOneRole : menuTreeListOfOneRole) {
				if(menuTree.getId().equals(menuTreeOfOneRole.getId())) {
					menuTree.setChecked(true);
				}
			}
		}
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = resp.getWriter();
		String tree = JSON.toJSONString(menuTreeOfAll);
		out.print(tree);
	}
}
