package ums.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import ums.service.UserService;
import util.SpringUtil;

/**
 * @author liangzhenghui
 * @date Aug 11, 20137:15:37 PM
 */
public class UserDeleteServlet extends HttpServlet {
	
	private static final long serialVersionUID = 4071728681327625356L;
	private Boolean result = false;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		JSONObject json = new JSONObject();
		String id = req.getParameter("id");
		UserService userService = UserService.getInstance();
		userService.deleteUser(id);
		result = true;
		json.put("result", result);
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = resp.getWriter();
	    out.println(json.toJSONString());
	}
}
