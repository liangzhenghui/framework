package ums.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import ums.service.FunctionService;
import ums.service.MenuService;
import util.SpringUtil;

/**
 * @author liangzhenghui
 * @date Aug 21, 2013    9:56:04 AM
 */
public class FunctionDeleteServlet extends HttpServlet {

	private static final long serialVersionUID = 9100962471137094302L;
	private Boolean result = false;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String functionId = req.getParameter("functionId");
		FunctionService functionService = FunctionService.getInstance();
		functionService.deleteFunction(functionId);
		result = true;
		JSONObject json = new JSONObject();
		json.put("result", result);
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = resp.getWriter();
	    out.println(json.toJSONString());
	}

}
