package ums.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import ums.service.RoleService;
import util.SpringUtil;

/**
 * @author liangzhenghui
 * @date Aug 27, 2013    12:31:08 PM
 */
public class RoleGrantUserServlet extends HttpServlet {

	private static final long serialVersionUID = -6360836543252363139L;
	private Boolean result = false;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		JSONObject json = new JSONObject();
		String userId = req.getParameter("userId");
		String roleId = req.getParameter("roleId");
		RoleService roleService = RoleService.getInstance();
		roleService.roleGrantUser(roleId, userId);
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = resp.getWriter();
		result = true;
		json.put("result", result);
		out.println(json.toJSONString());
	}

}
