package ums.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import ums.service.MenuService;
import util.SpringUtil;

/**
 * @author liangzhenghui
 * @date Aug 21, 2013    9:56:04 AM
 */
public class MenuCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 9100962471137094302L;
	private Boolean result = false;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		JSONObject json = new JSONObject();
		String parentMenu = req.getParameter("parentMenu");
		String menuName = req.getParameter("menuName");
		String functionId = req.getParameter("functionId");
		MenuService menuService = MenuService.getInstance();
		menuService.createMenu(menuName, parentMenu, functionId);
		result = true;
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = resp.getWriter();
		json.put("result", result);
	    out.println(json.toJSONString());
	}

}
