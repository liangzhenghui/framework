package ums.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import ums.model.Menu;
import ums.service.MenuService;
import util.SpringUtil;

/**
 * @author liangzhenghui
 * @date Aug 21, 2013    11:35:54 AM
 */
public class MenuEditServlet extends HttpServlet {

	
	private static final long serialVersionUID = 6698023905718559884L;
	private Boolean result = false;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		JSONObject json = new JSONObject();
		String menuId = req.getParameter("menuId");
		String parentMenu = req.getParameter("parentMenu");
		String functionId = req.getParameter("functionId");
		String menuName = req.getParameter("menuName");
		MenuService menuService = MenuService.getInstance();
		menuService.editMenu(menuId, functionId, menuName, parentMenu);
		result = true;
		json.put("result", result);
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = resp.getWriter();
	    out.println(json.toJSONString());
	}

}
