package ums.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ums.model.MenuTree;
import ums.model.User;
import ums.service.MenuService;
import ums.service.RoleService;
import util.SpringUtil;

import com.alibaba.fastjson.JSON;

/**
 * @author liangzhenghui
 * @date Aug 24, 2013    7:57:39 PM
 */
public class SystemMenuTreeServlet extends HttpServlet {
	
	private static final long serialVersionUID = 4315110956905406813L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		MenuService menuService =MenuService.getInstance();
		RoleService roleService = RoleService.getInstance();
		HttpSession session = req.getSession();
		User user = (User)session.getAttribute("user");
		List<MenuTree> menuTreeList = null;
		if(user != null) {
			List roleList = roleService.getRolesByUserId(user.getId());
			menuTreeList = menuService.getMenuTreeByRoles(roleList);
		}
		
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = resp.getWriter();
		String menuTree = JSON.toJSONString(menuTreeList);
		out.print(menuTree);
	}
}
