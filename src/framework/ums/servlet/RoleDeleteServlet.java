package ums.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ums.service.MenuService;
import ums.service.RoleService;
import util.SpringUtil;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
/**
 * 
 * @author liangzhenghui
 * @date Sep 14, 2013    08:44:27 AM
 */
public class RoleDeleteServlet extends HttpServlet {

	private static final long serialVersionUID = 8332895753484582404L;
	private Boolean result = false;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		JSONObject json = new JSONObject();
		String roleId = req.getParameter("roleId");
		RoleService roleService = RoleService.getInstance();
		roleService.deleteRole(roleId);
		result = true;
		json.put("result", result);
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = resp.getWriter();
	    out.println(json.toJSONString());
	}

}
