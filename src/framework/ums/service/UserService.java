package ums.service;

import java.sql.Types;
import java.util.List;
import java.util.UUID;

import ums.model.User;
import util.SpringUtil;
import dao.JdbcService;

/**
 * @author liangzhenghui
 * @date Aug 24, 2013    10:08:08 PM
 */
public class UserService {
	
	private JdbcService jdbcService;
	
	/**
	 * 使用迪米特法则,也称为最小知识原则，一个对象应该对其他的对象有最少的了解
	 * 不应该直接在Jsp或者servlet中使用SpringUtil.getBean("xxxx")获取这个bean的实例
	 * 而是通过UserService.getInstance()获取这个bean的实例
	 * 假如在jsp中使用了SpringUtil.getBean("xxxx")获取bean实例,万一有一天换了一种实现spring作用的框架;
	 * 那么就得在jsp或者servlet中大量的修改SpringUtil.getBean("xxxx")语句;
	 * 相反,假如使用xxxxService.getInstance()就不用修改。
	 * @return
	 */
	public static final UserService getInstance() {
		return (UserService)SpringUtil.getBean("userService");
	}
	
	public User getUserById(String id) {
		String sql = "select * from user u where u.id = ?";
		Object[] args = new Object[]{id};
		List list = jdbcService.queryForList(sql, args, new User());
		User user = null;
		if(list != null) {
			user = (User) list.get(0);
		}
		return user;
	}
	
	public User getUserByUserid(String userid) {
		String sql = "select * from user u where u.userid = ?";
		Object[] args = new Object[]{userid};
		List list = jdbcService.queryForList(sql, args, new User());
		User user = null;
		if(list != null) {
			user = (User) list.get(0);
		}
		return user;
	}
	
	public List getUsers() {
		String sql = "select * from user";
		Object[] args = new Object[]{};
		return jdbcService.queryForList(sql, args, new User());
	}
	
	public void createUser(String userid, String username) {
		String sql = "insert into user (id,userid,username) values(?,?,?)";
		Object[] args = new Object[]{UUID.randomUUID(),userid,username};
		int[] argTypes = new int[] { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };
		jdbcService.update(sql, args, argTypes);
	}
	
	public void editUser(String id, String userid, String username) {
		String sql = "update User u set userid = ?,username = ? where u.id = ? ";
		Object[] args = new Object[] { userid, username, id};
		int[] argTypes = new int[] {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		jdbcService.update(sql, args, argTypes);
	}
	
	public void deleteUser(String id) {
		String sql = "delete from user where id = ? ";
		Object[] args = new Object[] {id};
		int[] argTypes = new int[] {Types.VARCHAR};
		jdbcService.update(sql, args, argTypes);
		
		sql = "delete from user_role where userid = ? ";
		args = new Object[] {id};
		argTypes = new int[] {Types.VARCHAR};
		jdbcService.update(sql, args, argTypes);
	}
	
	/**判断账号是否已经存在
	 * @param userid
	 */
	public Boolean isExists(String userid) {
		String sql = "select count(*) from user where userid = ?";
		Object[] args = new Object[]{userid};
		int count = jdbcService.count(sql, args);
		if(count > 0) {
			return true;
		}
		else {
			return false;
		}
		
	}
	
	/**判断账号和姓名是否已经存在
	 * @param userid
	 */
	public Boolean isExists(String userid, String password) {
		String sql = "select count(*) from user where userid = ? and password = ?";
		Object[] args = new Object[]{userid, password};
		int count = jdbcService.count(sql, args);
		if(count > 0) {
			return true;
		}
		else {
			return false;
		}
		
	}
	
	/**
	 * @param username
	 * @return
	 */
	public User getUserByUsername(String username) {
		String sql = "select * from user where username = ? ";
		Object[] args = new Object[]{username};
		List list = jdbcService.queryForList(sql, args, new User());
		User user = null;
		if(list != null && list.size() != 0) {
			return (User)list.get(0);
		}
		return user;
	}
	
	public void setJdbcService(JdbcService jdbcService) {
		this.jdbcService = jdbcService;
	}

	public JdbcService getJdbcService() {
		return jdbcService;
	}

}
