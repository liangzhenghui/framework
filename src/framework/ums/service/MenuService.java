package ums.service;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;

import ums.model.Department;
import ums.model.Function;
import ums.model.Menu;
import ums.model.MenuTree;
import ums.model.Role;
import ums.model.RoleMenu;
import ums.model.UserRole;
import util.SpringUtil;
import dao.JdbcService;

/**
 * @author liangzhenghui
 * @date Aug 19, 2013 3:35:47 PM
 */
public class MenuService {
	private JdbcService jdbcService;

	/**
	 * 使用迪米特法则,也称为最小知识原则，一个对象应该对其他的对象有最少的了解
	 * 不应该直接在Jsp或者servlet中使用SpringUtil.getBean("xxxx")获取这个bean的实例
	 * 而是通过MenuService.getInstance()获取这个bean的实例
	 * 
	 * @return
	 */
	public static final MenuService getInstance() {
		return (MenuService) SpringUtil.getBean("menuService");
	}

	public List getAllMenu() {
		String sql = "select * from menu";
		Object[] args = new Object[] {};
		return jdbcService.queryForList(sql, args, new Menu());
	}

	public List getChildrenMenu(String parentMenu) {
		String sql = "select * from menu where parentmenu = ?";
		Object[] args = new Object[] { parentMenu };
		return jdbcService.queryForList(sql, args, new Menu());
	}

	public void createMenu(String menuName, String parentMenu, String functionId) {
		String sql = "insert into menu (menuid, menuname, parentmenu, functionid ) values(?,?,?,?)";
		Object[] args = new Object[] { UUID.randomUUID(), menuName, parentMenu,
				functionId };
		int[] argTypes = new int[] { Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR, Types.VARCHAR };
		jdbcService.update(sql, args, argTypes);
	}

	/**
	 * @param menuId
	 */
	public Menu getMenuById(String menuId) {
		String sql = "select * from menu t where t.menuid = ? ";
		Object[] args = new Object[] { menuId };
		List list = jdbcService.queryForList(sql, args, new Menu());
		Menu menu = null;
		if (list != null && list.size() != 0) {
			menu = (Menu) list.get(0);
		}
		return menu;
	}

	public void editMenu(String menuId, String functionId, String menuName,
			String parentMenu) {
		String sql = "update menu  set  functionid = ? , menuname = ? ,parentmenu = ? where menuid = '"
				+ menuId + "'";
		Object[] args = new Object[] { functionId, menuName, parentMenu };
		int[] argTypes = new int[] { Types.VARCHAR, Types.VARCHAR,
				Types.VARCHAR };
		jdbcService.update(sql, args, argTypes);
	}

	/**
	 * 应该删除所有的子菜单
	 * 
	 * @param menuId
	 */
	public void deleteMenu(String menuId) {
		String sql = "delete from menu where menuid = ? ";
		Object[] args = new Object[] { menuId };
		int[] argTypes = new int[] { Types.VARCHAR };
		jdbcService.update(sql, args, argTypes);
		// 删除一条菜单的同时,应该将menu_role表中包含该菜单的数据删掉,避免脏数据或者因为id缺失导致的错误。
		sql = "delete from role_menu where menuid = ?";
		args = new Object[] { menuId };
		argTypes = new int[] { Types.VARCHAR };
		jdbcService.update(sql, args, argTypes);
		sql = "select * from menu where parentmenu = ?";
		List list = jdbcService.queryForList(sql, args, new Menu());
		if (list != null && list.size() > 0) {
			for (Object object : list) {
				Menu menu = (Menu) object;
				if (menu != null) {
					deleteMenu(menu.getMenuId());
				}
			}
		}
	}

	/**
	 * 取出所有的菜单，并将菜单放进MenuTree以List<MenuTree> 的List形式返回去
	 * 
	 * @return
	 */
	public List<MenuTree> getMenuTree() {
		List menuList = getAllMenu();
		List<MenuTree> menuTreeList = new ArrayList<MenuTree>();
		for (Object object : menuList) {
			Menu menu = (Menu) object;
			MenuTree menuTree = new MenuTree(menu.getMenuId(),
					menu.getParentMenu(), menu.getMenuName());
			menuTreeList.add(menuTree);
		}
		return menuTreeList;
	}

	/**
	 * @param roleId
	 * @param menuIds
	 */
	public void grantMenuToRole(String roleId, String[] menuIds) {
		String sql = "";
		// 每次为角色添加权限的时候都先将改角色原来所有的权限全部删掉
		sql = "delete from role_menu where roleid = ?";
		Object[] args1 = new Object[] { roleId };
		int[] argTypes1 = new int[] { Types.VARCHAR };
		jdbcService.update(sql, args1, argTypes1);
		for (String menuId : menuIds) {
			sql = "insert into role_menu (id, roleid, menuid) values(?, ?, ?)";
			Object[] args = new Object[] { UUID.randomUUID(), roleId, menuId };
			int[] argTypes = new int[] { Types.VARCHAR, Types.VARCHAR,
					Types.VARCHAR };
			jdbcService.update(sql, args, argTypes);
		}
	}

	public List getMenusByRoleId(String roleId) {
		String sql = "select * from role_menu where roleid = ?";
		Object[] args = new Object[] { roleId };
		int[] argTypes = new int[] { Types.VARCHAR };
		List roleMenus = jdbcService.queryForList(sql, args, new RoleMenu());
		List<Menu> menuList = new ArrayList<Menu>();
		for (Object object : roleMenus) {
			RoleMenu roleMenu = (RoleMenu) object;
			menuList.add(getMenuById(roleMenu.getMenuId()));
		}
		return menuList;
	}

	public Boolean hasChildren(String menuId) {
		String sql = "select * from menu where parentmenu = ?";
		Object[] args = new Object[] { menuId };
		List list = jdbcService.queryForList(sql, args, new Menu());
		if (list != null && list.size() > 0) {
			return true;
		} else
			return false;
	}

	public JdbcService getJdbcService() {
		return jdbcService;
	}

	public void setJdbcService(JdbcService jdbcService) {
		this.jdbcService = jdbcService;
	}

	/**
	 * @param roleList
	 * @return
	 */
	public List<MenuTree> getMenuTreeByRoles(List roleList) {
		List<MenuTree> menuTrees = new ArrayList<MenuTree>();
		MenuService menuService = (MenuService) SpringUtil
				.getBean("menuService");
		FunctionService functionService = (FunctionService) SpringUtil
				.getBean("functionService");
		for (Object object : roleList) {
			Role role = (Role) object;
			List menuList = menuService.getMenusByRoleId(role.getRoleId());
			for (Object object1 : menuList) {
				Menu menu = (Menu) object1;
				MenuTree menuTree1 = new MenuTree();
				menuTree1.setId(menu.getMenuId());
				menuTree1.setMenuName(menu.getMenuName());
				menuTree1.setParentMenu(menu.getParentMenu());
				String url = "";
				// 如果是父菜单则将url设置为#
				if(menuService.hasChildren(menu.getMenuId())) {
					url = "#";
				}
				else{
					if (StringUtils.isNotBlank(menu.getFunctionId())) {
						Function function = functionService.getFunctionById(menu
								.getFunctionId());
						url = function.getUrl();
					}
				}
				menuTree1.setUrl(url);
				menuTrees.add(menuTree1);
			}
		}
		return menuTrees;
	}

	// 根据角色ID查找该角色下的所有菜单
	public List<MenuTree> getMenuTreeByRoleId(String roleId) {
		List<MenuTree> menuTrees = new ArrayList<MenuTree>();
		MenuService menuService = (MenuService) SpringUtil
				.getBean("menuService");
		FunctionService functionService = (FunctionService) SpringUtil
				.getBean("functionService");
		List menuList = menuService.getMenusByRoleId(roleId);
		for (Object object1 : menuList) {
			Menu menu = (Menu) object1;
			MenuTree menuTree1 = new MenuTree();
			menuTree1.setId(menu.getMenuId());
			menuTree1.setMenuName(menu.getMenuName());
			menuTree1.setParentMenu(menu.getParentMenu());
			String url = "";
			if (StringUtils.isNotBlank(menu.getFunctionId())) {
				Function function = functionService.getFunctionById(menu
						.getFunctionId());
				url = function.getUrl();
			}
			menuTree1.setUrl(url);
			menuTrees.add(menuTree1);
		}
		return menuTrees;
	}

}
