package ums.service;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import ums.model.Department;
import ums.model.DepartmentTree;
import ums.model.Menu;
import ums.model.MenuTree;
import util.SpringUtil;
import dao.JdbcService;

/**
 * 
 * @author liangzhenghui
 * 2013-11-23
 */
public class DepartmentService {
	
	private JdbcService jdbcService;
	
	/**
	 * 使用迪米特法则,不应该直接在Jsp或者servlet中使用SpringUtil.getBean("xxxx")获取这个bean的实例
	 * 而是通过DepartmentService.getInstance()获取这个bean的实例
	 * @return
	 */
	public static final DepartmentService getInstance() {
		return (DepartmentService)SpringUtil.getBean("departmentService");
	}
	
	public List getAllDepartments() {
		String sql = "select * from department";
		Object[] args = new Object[]{};
		return jdbcService.queryForList(sql, args, new Department());
	}
	
	public Department getDepartmentById(String id) {
		String sql = "select * from department where id = '"+id+"'";
		List list = jdbcService.queryForList(sql, new Object[]{},new Department());
		Department department = null;
		if(list != null && list.size() != 0) {
			department = (Department)list.get(0);
		}
		return department;
	}
	
	
	public void createDepartment(String pid, String name) {
		String sql = "insert into department (id, pid, name) values(?,?,?)";
		Object [] args = new Object[]{UUID.randomUUID(), pid, name};
		int[] argTypes = new int[] {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		jdbcService.update(sql, args, argTypes);
		
	}
	/**
	 * 是否删除一个机构的时候也将下面的孩子机构全部删掉呢？
	 * @param id
	 */
	public void deleteDepartment(String id) {
		String sql = "delete  from department where id = ? ";
		Object[] args = new Object[]{id};
		int [] argTypes = new int[]{Types.VARCHAR};
		jdbcService.update(sql, args, argTypes);
		sql = "select * from department where pid = ?";
		List list = jdbcService.queryForList(sql, args, new Department());
		if(list != null && list.size() > 0) {
			for(Object object : list) {
				Department department = (Department)object;
				if(department != null) {
					deleteDepartment(department.getId());
				}
			}
		}
		
	}

	public void editDepartment(String id, String pid, String name) {
		String sql = "update department set pid = ? , name = ? where id = '"+id+"'";
		Object [] args = new Object[]{pid, name};
		int[] argTypes = new int[] {Types.VARCHAR, Types.VARCHAR};
		jdbcService.update(sql, args, argTypes);
	}
	
	
	/**
	 * 取出所有的菜单，并将菜单放进DepartmentTree以List<DepartmentTree> 的List形式返回去
	 * @return
	 */
	public List<DepartmentTree> getDepartmentTree() {
		List departmentList = getAllDepartments(); 
		List<DepartmentTree> departmentTreeList = new ArrayList<DepartmentTree>();
		for(Object object : departmentList) {
			Department department = (Department)object;
			DepartmentTree departmentTree = new DepartmentTree(department.getId(), department.getPid(), department.getName());
			departmentTreeList.add(departmentTree);
		}
		return departmentTreeList;
	}
	

	public JdbcService getJdbcService() {
		return jdbcService;
	}

	public void setJdbcService(JdbcService jdbcService) {
		this.jdbcService = jdbcService;
	}
	

}
