package ums.service;

import java.sql.Types;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;

import ums.model.Function;
import util.SpringUtil;
import dao.JdbcService;

/**
 * @author liangzhenghui
 * @date Aug 20, 2013    2:53:21 PM
 */
public class FunctionService {

	private JdbcService jdbcService;
	

	/**
	 * 使用迪米特法则,也称为最小知识原则，一个对象应该对其他的对象有最少的了解
	 * 不应该直接在Jsp或者servlet中使用SpringUtil.getBean("xxxx")获取这个bean的实例
	 * 而是通过FunctionService.getInstance()获取这个bean的实例
	 * @return
	 */
	public static final FunctionService getInstance() {
		return (FunctionService)SpringUtil.getBean("functionService");
	}
	
	public List getAllFunction() {
		String sql = "select * from function";
		Object[] args = new Object[]{};
		return jdbcService.queryForList(sql, args, new Function());
	}
	
	/**
	 * @param menuName
	 * @param parentMenu
	 */
	public void createFunction(String functionName, String url) {
		String sql = " insert into function (functionid, functionname, url) values(?,?,?)";
		Object[] args = new Object[]{UUID.randomUUID(),functionName,url };
		int[] argTypes = new int[] {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		jdbcService.update(sql, args, argTypes);
		
	}
	
	public Function getFunctionById(String functionId) {
		String sql = "select * from function where functionid = ? ";
		Object[] args =new Object[]{functionId};
		List list = jdbcService.queryForList(sql, args, new Function());
		Function function = null;
		if(list != null && list.size() != 0) {
			function =  (Function)list.get(0);
		}
		return function;
	}
	
	/**
	 * @param functionName
	 * @param url
	 */
	public void editFunction(String functionId, String functionName, String url) {
		String sql = "update function set functionname = ?, url = ? where functionid = ?";
		Object[] args = new Object[]{functionName, url, functionId};
		int[] argTypes = new int[] {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		jdbcService.update(sql, args, argTypes);
	}
	
	/**
	 * @param functionId
	 */
	public void deleteFunction(String functionId) {
		String sql = "delete  from function where  functionid = ? ";
		Object[] args = new Object[]{functionId};
		int [] argTypes = new int[] {Types.VARCHAR};
		jdbcService.update(sql, args, argTypes);
		
	}
	
	public JdbcService getJdbcService() {
		return jdbcService;
	}

	public void setJdbcService(JdbcService jdbcService) {
		this.jdbcService = jdbcService;
	}

}
