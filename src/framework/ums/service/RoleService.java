package ums.service;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import ums.model.Menu;
import ums.model.Role;
import ums.model.UserRole;
import util.SpringUtil;
import dao.JdbcService;

/**
 * @author liangzhenghui
 * @date Aug 24, 2013    9:59:08 PM
 */
public class RoleService {
	
	private JdbcService jdbcService;

	/**
	 * 使用迪米特法则,也称为最小知识原则，一个对象应该对其他的对象有最少的了解
	 * 不应该直接在Jsp或者servlet中使用SpringUtil.getBean("xxxx")获取这个bean的实例
	 * 而是通过MenuService.getInstance()获取这个bean的实例
	 * @return
	 */
	public static final RoleService getInstance() {
		return (RoleService)SpringUtil.getBean("roleService");
	}
	
	public void createRole(String roleName) {
		String sql = "insert into role (roleid,rolename) values(?, ?)";
		Object[] args = new Object[]{UUID.randomUUID(), roleName};
		int [] argTypes = new int[]{Types.VARCHAR, Types.VARCHAR};
		jdbcService.update(sql, args, argTypes);
	}
	
	/**
	 * @param roleId
	 * @param userId
	 */
	public void roleGrantUser(String roleId, String userId) {
		String sql = "insert into user_role (id, userid, roleid) values(?, ?, ?)";
		Object[] args = new Object[] {UUID.randomUUID(), userId, roleId};
		int[] argTypes = new int[]{Types.VARCHAR, Types.VARCHAR, Types.VARCHAR};
		jdbcService.update(sql, args, argTypes);
	}
	
	public List getAllRoles() {
		String sql = "select * from role";
		Object [] args = new Object[] {};
		return jdbcService.queryForList(sql, args ,new Role());
	}
	
	public List getRolesByUserId(String userId) {
		String sql = "select * from user_role where userid = ?";
		Object[] args = new Object[]{userId};
		List list = jdbcService.queryForList(sql, args, new UserRole());
		List<Role> roleList = new ArrayList<Role>();
		for(Object object : list) {
			UserRole userRole = (UserRole)object;
			roleList.add(getRoleById(userRole.getRoleId()));
		}
		return roleList;
	}
	
	/**
	 * @param string
	 * @return
	 */
	public Role getRoleById(String roleId) {
		String sql = "select * from Role t where t.roleid = ? ";
		Object[] args =new Object[]{roleId};
		List list =  jdbcService.queryForList(sql, args, new Role());
		if(list != null && list.size() != 0) {
			return (Role)list.get(0);
		}
		else return null;
	}
	/**
	 * 删除角色,同时删除user_role,role_menu中所有包含roleId的记录
	 * @param roleId
	 */
	public void deleteRole(String roleId) {
		//删除role表单中与roleId有关的记录
		String sql =  null;
		Object[] args = null;
		int[] argTypes = null;
		sql = "delete from role  where roleid = ? ";
		args = new Object[]{roleId};
		argTypes = new int[]{Types.VARCHAR};
		jdbcService.update(sql, args, argTypes);
		//删除role_menu表单中的所有包含roleId的记录
		sql = "delete from role_menu  where roleid = ? ";
		jdbcService.update(sql, args, argTypes);
		//删除user_role表单中所有包含roleId的记录
		sql = "delete from user_role  where roleid = ? ";
		jdbcService.update(sql, args, argTypes);
	}
	

	public JdbcService getJdbcService() {
		return jdbcService;
	}

	public void setJdbcService(JdbcService jdbcService) {
		this.jdbcService = jdbcService;
	}

}
