package ums.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

/**
 * @author liangzhenghui
 * @date Aug 20, 2013    3:32:12 PM
 */
public class Menu implements RowMapper{
	private String menuId;
	private String menuName;
	private String  parentMenu;
	private String functionId;
	
	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getParentMenu() {
		return parentMenu;
	}

	public void setParentMenu(String parentMenu) {
		this.parentMenu = parentMenu;
	}

	public String getFunctionId() {
		return functionId;
	}

	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		Menu menu = new Menu();
		menu.setMenuId(rs.getString("MENUID"));
		menu.setMenuName(rs.getString("MENUNAME"));
		menu.setParentMenu(rs.getString("PARENTMENU"));
		menu.setFunctionId(rs.getString("FUNCTIONID"));
		return  menu;
	}
}
