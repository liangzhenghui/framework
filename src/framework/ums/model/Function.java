package ums.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

/**
 * @author liangzhenghui
 * @date Aug 20, 2013    3:32:12 PM
 */
public class Function implements RowMapper{
	private String functionId;
	
	private String functionName;
	
	private String url;

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		Function function= new Function();
		function.setFunctionId(rs.getString("FUNCTIONID"));
		function.setFunctionName(rs.getString("FUNCTIONNAME"));
		function.setUrl(rs.getString("URL"));
		return function;
	}

	public String getFunctionId() {
		return functionId;
	}

	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
