package service;

public class HelloService {
	private HelloJdbcService helloJdbcService;

	public void setHelloJdbcService(HelloJdbcService helloJdbcService) {
		this.helloJdbcService = helloJdbcService;
	}

	public HelloJdbcService getHelloJdbcService() {
		return helloJdbcService;
	}
}
