package backingbean;

public class UserBackingBean {
	
	private String username="My name is liangzhenghui";
	
	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}
}
