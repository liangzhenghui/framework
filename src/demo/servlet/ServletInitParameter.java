package servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * 测试InitParameter
 * @author ken
 *
 */
public class ServletInitParameter extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7094673076240375858L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		System.out.println("你进入了init(arg)方法"+config.getInitParameter("emailAdress")+"---");
		super.init(config);//注释掉这一句，下面的getServletConfig()将返回是null
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		System.out.println("aaaa"+this.getServletConfig().getInitParameter("emailAdress"));
	}

}
