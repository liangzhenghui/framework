package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.UserService;
import util.SpringUtil;

/**
 * @author liangzhenghui
 * @date Aug 11, 20137:15:37 PM
 */
public class UserEditServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4071728681327625356L;
	

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String username = req.getParameter("username");
		String id = req.getParameter("id");
		UserService userService = (UserService)SpringUtil.getBean("userService");
		userService.editUser(username,id);
		resp.setCharacterEncoding("UTF-8");
		PrintWriter out = resp.getWriter();
	    out.println("编辑成功!");
	}
}
