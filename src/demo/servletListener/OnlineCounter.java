package servletListener;


import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;

public class OnlineCounter implements HttpSessionListener{

	private static int sessionCounter = 0;
	
	 private static Logger log = Logger.getLogger(OnlineCounter.class);
	
	 public OnlineCounter() {
		 log.info("onlineCounter initialized");
	 }
	 
	 @Override
	public void sessionCreated(HttpSessionEvent arg0) {
		sessionCounter++;
		log.info("session created:"+sessionCounter);
		
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
		sessionCounter--;
		log.info("session destoried");
	}
	
	public static int getOnlineSession() {
		return sessionCounter;
	}

}
